/*
 * Copyright (c) 2015 Eneim Labs. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations
 * under the License.
 */

package im.ene.lab.design.sample;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import im.ene.lab.design.widget.swipecards.ArrayStackAdapter;
import im.ene.lab.design.widget.swipecards.StackView;
import im.ene.lab.design.widget.swipecards.TinderView;

import java.util.ArrayList;

public class MyActivity extends Activity {

  @Bind(R.id.frame)
  StackView mFlingView;
  private ArrayList<String> al;
  private ArrayStackAdapter<String> arrayAdapter;
  private int i;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_my);
    ButterKnife.bind(this);

    al = new ArrayList<>();
    for (int i = 0; i < 13; i++) {
      al.add((i + 1) + "");
    }
    i = al.size();

    arrayAdapter = new ArrayStackAdapter<>(this, R.layout.item, R.id.helloText, al);

    mFlingView.setAdapter(arrayAdapter);
    mFlingView.setOnSwipeListener(new StackView.OnCardSwipeListener() {
      @Override
      public void onExiting(View view, float offset) {
        if (view != null) {
          view.findViewById(R.id.item_swipe_right_indicator).setAlpha(offset < 0 ? -offset : 0);
          view.findViewById(R.id.item_swipe_left_indicator).setAlpha(offset > 0 ? offset : 0);
        }
      }

      @Override
      public void onExited(View view) {
        // this is the simplest way to delete an object from the Adapter (/AdapterView)
        Log.d("LIST", "removed object!");
      }

      @Override
      public void onExitToLeft(View dataObject) {
        //Do something on the left!
        //You also have access to the original object.
        //If you want to use it just cast it (String) dataObject
        makeToast(MyActivity.this, "Left!");
      }

      @Override
      public void onExitToRight(View dataObject) {
        makeToast(MyActivity.this, "Right!");
      }

      @Override
      public void onAdapterAboutToEmpty(int count) {
        // Ask for more data here
//        al.add((i + 1) + "");
//        arrayAdapter.notifyDataSetChanged();
        Log.d("LIST", "notified");
        i++;
      }
    });

    // Optionally add an OnItemClickListener
    mFlingView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        makeToast(MyActivity.this, "Clicked!");
        mFlingView.swipeToCard(5);
      }
    });

  }

  static void makeToast(Context ctx, String s) {
    Toast.makeText(ctx, s, Toast.LENGTH_SHORT).show();
  }

  @OnClick(R.id.right)
  public void right() {
    /**
     * Trigger the right event manually.
     */
    mFlingView.swipeToRight();
//    mFlingView.getSelectedView();
  }

  @OnClick(R.id.left)
  public void left() {
    mFlingView.swipeToLeft();
  }

  @OnClick(R.id.undo)
  public void undo() {
    mFlingView.undo();
  }

  public static class MyStyle extends TinderView.Style {

    public MyStyle() {

    }

    public MyStyle(Context context, AttributeSet attrs) {
      super(context, attrs);
    }

    @Override
    protected void layoutNewChild(View child, int index, int maxIndex) {
      super.layoutNewChild(child, index, maxIndex);
    }

    @Override
    protected void flingTopChild(float offset, int topIndex, View otherView, int otherViewIndex) {
      super.flingTopChild(offset, topIndex, otherView, otherViewIndex);
    }
  }

}
