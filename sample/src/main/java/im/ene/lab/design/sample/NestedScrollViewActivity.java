/*
 * Copyright (c) 2015 Eneim Labs. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations
 * under the License.
 */

package im.ene.lab.design.sample;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Switch;

import im.ene.lab.design.widget.vector.AnimatedVectorDrawable;
import im.ene.lab.design.widget.vector.PathAnimatorInflater;
import im.ene.lab.design.widget.vector.PathParser;
import im.ene.lab.design.widget.vector.VectorDrawable;

public class NestedScrollViewActivity extends AppCompatActivity {

  VectorDrawable drawable;
  AnimatedVectorDrawable animatedVectorDrawable;

  android.graphics.drawable.AnimatedVectorDrawable sample;

  ImageView testImage;
  Switch mSwitch;

  private VectorDrawable.VFullPath mSunPath;
  private PathAnimatorInflater.PathDataEvaluator pathEvaluatorSun2Moon;
  private PathParser.PathDataNode[] currentSunDataPath;

  @TargetApi(Build.VERSION_CODES.LOLLIPOP)
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_nested_scroll_view);

    ImageView image = (ImageView) findViewById(R.id.image);
//        drawable = image.getDrawable();
    drawable = VectorDrawable.getDrawable(this, R.drawable.design__switch_day_night_day);
    animatedVectorDrawable = AnimatedVectorDrawable.getDrawable(this, R.drawable
        .animated_background_border);

    testImage = (ImageView) findViewById(R.id.test);
    testImage.setImageDrawable(drawable);
  }

  @Override
  protected void onResume() {
    super.onResume();
    mSunPath = (VectorDrawable.VFullPath) drawable.getTargetByName("design__day_sun");
    VectorDrawable nightDrawable = VectorDrawable.getDrawable(this, im.ene.lab.design.R.drawable
        .design__switch_day_night_night);

    final VectorDrawable.VFullPath moonPath = (VectorDrawable.VFullPath) nightDrawable
        .getTargetByName("design__night_moon");
    pathEvaluatorSun2Moon = new PathAnimatorInflater.PathDataEvaluator(currentSunDataPath);

    testImage.postDelayed(new Runnable() {
      @Override
      public void run() {
        PathParser.PathDataNode[] newNodes = pathEvaluatorSun2Moon.evaluate(0.75f,
            PathParser.deepCopyNodes(mSunPath.getPathData()),
            PathParser.deepCopyNodes(moonPath.getPathData()));

        mSunPath.setPathData(newNodes);
        drawable.updatePath(mSunPath);
        testImage.invalidate();
      }
    }, 1000);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_nested_scroll_view, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_settings) {
      return true;
    }

    return super.onOptionsItemSelected(item);
  }

}
