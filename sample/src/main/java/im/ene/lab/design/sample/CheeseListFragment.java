/*
 * Copyright (c) 2015 Eneim Labs. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations
 * under the License.
 */

package im.ene.lab.design.sample;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.du.android.recyclerview.SwipeToDismissTouchListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CheeseListFragment extends Fragment {

  public static final String TAG = "CheeseListFragment";
  static final int INVALID_POINTER_ID = -1;
  static final int TOUCH_ABOVE = 0;
  static final int TOUCH_BELOW = 1;
  private float mPosX;
  private float mPosY;
  private float mDownTouchX;
  private float mDownTouchY;
  private View mFrame = null;
  private int mFrameHeight;
  private int mFrameWidth;
  private int mTouchPosition;
  private float mBaseRotateDegree = 15.f;
  private int mActivePointerId = INVALID_POINTER_ID;
  private float mFrameX;
  private float mFrameY;
  private int mParentWidth;
  private RecyclerView.Adapter mAdapter;

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
      savedInstanceState) {
    RecyclerView rv = (RecyclerView) inflater.inflate(
        R.layout.fragment_cheese_list, container, false);
    setupRecyclerView(rv);
    return rv;
  }

  private void setupRecyclerView(RecyclerView recyclerView) {
    recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
    final List<String> items = getRandomSublist(Cheeses.sCheeseStrings, 30);
    mAdapter = new SimpleStringRecyclerViewAdapter(getActivity(), items);
    recyclerView.setAdapter(mAdapter);
    recyclerView.addOnItemTouchListener(new SwipeToDismissTouchListener(recyclerView,
        new SwipeToDismissTouchListener.DismissCallbacks() {
          @Override public SwipeToDismissTouchListener.SwipeDirection canDismiss(int position) {
            return SwipeToDismissTouchListener.SwipeDirection.RIGHT;
          }

          @Override
          public void onDismiss(RecyclerView view,
                                List<SwipeToDismissTouchListener.PendingDismissData> dismissData) {
            for (SwipeToDismissTouchListener.PendingDismissData data : dismissData) {
              items.remove(data.position);
              mAdapter.notifyItemRemoved(data.position);
            }
          }
        }));
  }

  private List<String> getRandomSublist(String[] array, int amount) {
    ArrayList<String> list = new ArrayList<>(amount);
    Random random = new Random();
    while (list.size() < amount) {
      list.add(array[random.nextInt(array.length)]);
    }
    return list;
  }

  private void onTouchUpAction() {

  }

  private float getFlingOffset() {
    return 0.f;
  }

  public void onFlingTopView(float offset) {

  }

  public static class SimpleStringRecyclerViewAdapter
      extends RecyclerView.Adapter<SimpleStringRecyclerViewAdapter.ViewHolder> {

    private final TypedValue mTypedValue = new TypedValue();
    private int mBackground;
    private List<String> mValues;

    public SimpleStringRecyclerViewAdapter(Context context, List<String> items) {
      context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
      mBackground = mTypedValue.resourceId;
      mValues = items;
    }

    public String getValueAt(int position) {
      return mValues.get(position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      View view = LayoutInflater.from(parent.getContext())
          .inflate(R.layout.list_item, parent, false);
      view.setBackgroundResource(mBackground);
      return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
      holder.mBoundString = mValues.get(position);
      holder.mTextView.setText(mValues.get(position));
      holder.mView.setClickable(true);

      Glide.with(holder.mImageView.getContext())
          .load(Cheeses.getRandomCheeseDrawable())
          .fitCenter()
          .into(holder.mImageView);
    }

    @Override
    public int getItemCount() {
      return mValues.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
      public final View mView;
      public final ImageView mImageView;
      public final TextView mTextView;
      public String mBoundString;

      public ViewHolder(View view) {
        super(view);
        mView = view;
        mImageView = (ImageView) view.findViewById(R.id.avatar);
        mTextView = (TextView) view.findViewById(android.R.id.text1);
      }

      @Override
      public String toString() {
        return super.toString() + " '" + mTextView.getText();
      }
    }
  }
}
