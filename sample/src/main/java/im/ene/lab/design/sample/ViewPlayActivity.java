/*
 * Copyright (c) 2015 Eneim Labs. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations
 * under the License.
 */

package im.ene.lab.design.sample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import im.ene.lab.design.widget.layout.HorizontalCardLayoutManager;
import im.ene.lab.design.widget.layout.SnappyRecyclerView;

public class ViewPlayActivity extends AppCompatActivity {

  SnappyRecyclerView mRecyclerView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_view_play);
    mRecyclerView = (SnappyRecyclerView) findViewById(R.id.recyclerview);
  }

  @Override
  protected void onResume() {
    super.onResume();
    mRecyclerView.setLayoutManager(new HorizontalCardLayoutManager(this));
    mRecyclerView.setHasFixedSize(true);
    mRecyclerView.setAdapter(new DummyAdapter());
  }

  class DummyAdapter extends RecyclerView.Adapter {

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
      View view = LayoutInflater.from(ViewPlayActivity.this).inflate(R.layout.list_item_2,
          viewGroup, false);
      return new RecyclerView.ViewHolder(view) {
      };
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
      TextView textView = (TextView) viewHolder.itemView.findViewById(R.id.text);
      if (textView != null)
        textView.setText(i + "");
    }

    @Override
    public int getItemCount() {
      return 100;
    }
  }
}
