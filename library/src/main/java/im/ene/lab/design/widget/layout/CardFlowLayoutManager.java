package im.ene.lab.design.widget.layout;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.ViewGroup;

/**
 * Created by eneim on 7/28/15.
 */
public class CardFlowLayoutManager extends RecyclerView.LayoutManager {

  @Override
  public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {

  }

  @Override
  public RecyclerView.LayoutParams generateDefaultLayoutParams() {
    return new LayoutParams(
        RecyclerView.LayoutParams.WRAP_CONTENT,
        RecyclerView.LayoutParams.WRAP_CONTENT);
  }

  @Override
  public boolean canScrollHorizontally() {
    return true;
  }

  @Override
  public boolean canScrollVertically() {
    return false;
  }

  public static class LayoutParams extends RecyclerView.LayoutParams {

    public LayoutParams(Context c, AttributeSet attrs) {
      super(c, attrs);
    }

    public LayoutParams(int width, int height) {
      super(width, height);
    }

    public LayoutParams(ViewGroup.MarginLayoutParams source) {
      super(source);
    }

    public LayoutParams(ViewGroup.LayoutParams source) {
      super(source);
    }

    public LayoutParams(RecyclerView.LayoutParams source) {
      super(source);
    }
  }
}
