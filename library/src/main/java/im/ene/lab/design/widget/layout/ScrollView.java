/*
 * Copyright (c) 2015 Eneim Labs. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations
 * under the License.
 */

package im.ene.lab.design.widget.layout;

import android.content.Context;
import android.support.v4.widget.NestedScrollView;
import android.util.AttributeSet;

import im.ene.lab.design.Designer;

/**
 * Created by eneim on 7/16/15.
 */
public class ScrollView extends NestedScrollView implements Designer.Scrollable {

  public ScrollView(Context context) {
    super(context);
  }

  public ScrollView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public ScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  @Override
  public int getHorizontalScrollRange() {
    return computeHorizontalScrollRange();
  }

  @Override
  public int getHorizontalScrollOffset() {
    return computeHorizontalScrollOffset();
  }

  @Override
  public int getHorizontalScrollExtent() {
    return computeHorizontalScrollExtent();
  }

  @Override
  public int getVerticalScrollRange() {
    return computeVerticalScrollRange();
  }

  @Override
  public int getVerticalScrollOffset() {
    return computeVerticalScrollOffset();
  }

  @Override
  public int getVerticalScrollExtent() {
    return computeVerticalScrollExtent();
  }
}
