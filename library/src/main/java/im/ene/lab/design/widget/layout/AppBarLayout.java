/*
 * Copyright (c) 2015 Eneim Labs. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations
 * under the License.
 */

package im.ene.lab.design.widget.layout;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by eneim on 7/16/15.
 */
public class AppBarLayout extends android.support.design.widget.AppBarLayout {

  CoordinatorLayout tester;

  public AppBarLayout(Context context) {
    super(context);
  }

  public AppBarLayout(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public static class ScrollingViewBehavior extends
      android.support.design.widget.AppBarLayout.ScrollingViewBehavior {

    public ScrollingViewBehavior() {
      super();
    }

    public ScrollingViewBehavior(Context context, AttributeSet attributeSet) {
      super(context, attributeSet);
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, View child, View dependency) {
      return super.layoutDependsOn(parent, child, dependency) || dependency instanceof AppBarLayout;
    }

    @Override
    public boolean onMeasureChild(CoordinatorLayout parent, View child, int
        parentWidthMeasureSpec, int widthUsed, int parentHeightMeasureSpec, int heightUsed) {
      return super.onMeasureChild(parent, child, parentWidthMeasureSpec, widthUsed,
          parentHeightMeasureSpec, heightUsed);
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, View child, View dependency) {
      return super.onDependentViewChanged(parent, child, dependency);
    }
  }

}
