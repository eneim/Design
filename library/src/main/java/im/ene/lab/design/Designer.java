/*
 * Copyright (c) 2015 Eneim Labs. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations
 * under the License.
 */

package im.ene.lab.design;

/**
 * Created by eneim on 7/16/15.
 * <p/>
 * A Helper class for utilities
 */
public class Designer {

  /**
   * Generic callback for general purpose
   */
  public interface Callback {

    /**
     * @param isSuccess
     */
    void onDone(boolean isSuccess);
  }

  public interface Scrollable {

    int getHorizontalScrollRange();

    int getHorizontalScrollOffset();

    int getHorizontalScrollExtent();

    int getVerticalScrollRange();

    int getVerticalScrollOffset();

    int getVerticalScrollExtent();
  }
}
