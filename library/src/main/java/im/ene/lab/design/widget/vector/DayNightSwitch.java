/*
 * Copyright (c) 2015 Eneim Labs. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations
 * under the License.
 */

package im.ene.lab.design.widget.vector;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Property;
import android.view.MotionEvent;
import android.view.SoundEffectConstants;
import android.view.VelocityTracker;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;

import im.ene.lab.design.R;

/**
 * Created by eneim on 7/23/15.
 */
public class DayNightSwitch extends CompoundButton {

  private static final int THUMB_ANIMATION_DURATION = 250;
  private static final int TOUCH_MODE_IDLE = 0;
  private static final int TOUCH_MODE_DOWN = 1;
  private static final int TOUCH_MODE_DRAGGING = 2;
  private final String TAG = DayNightSwitch.class.getSimpleName();
  Switch refSwitch;
  ImageView refImageView;
  private int mTouchMode;
  private int mTouchSlop;
  private float mTouchX;
  private float mTouchY;
  private VelocityTracker mVelocityTracker = VelocityTracker.obtain();
  private int mMinFlingVelocity;

  private float mThumbPosition;
  private static final Property<DayNightSwitch, Float> SUN_POS = new Property<DayNightSwitch,
      Float>(Float.class, "sun_pos") {
    @Override
    public Float get(DayNightSwitch object) {
      return object.mThumbPosition;
    }
  };
  private VectorDrawable mDrawable;
  //    private VectorDrawable.VGroup mCloudGroup;
  // components
  private VectorDrawable.VFullPath mSunPath;
  // animators
  private ValueAnimator mPathAnimator;        // animator for path value
  private ObjectAnimator mPositionAnimator;   // animator for position
  private PathAnimatorInflater.PathDataEvaluator pathEvaluatorSun2Moon;
  private PathParser.PathDataNode[] currentSunDataPath;

  public DayNightSwitch(Context context) {
    this(context, null);
  }

  public DayNightSwitch(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public DayNightSwitch(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init(context, attrs, defStyleAttr, 0);
  }

  private void init(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
    mDrawable = VectorDrawable.getDrawable(context, R.drawable
        .design__switch_day_night_day);

    mSunPath = (VectorDrawable.VFullPath) mDrawable.getTargetByName("design__day_sun");

    VectorDrawable nightDrawable = VectorDrawable.getDrawable(context, R.drawable
        .design__switch_day_night_night);

    VectorDrawable.VFullPath moonPath = (VectorDrawable.VFullPath) nightDrawable.getTargetByName
        ("design__night_moon");
    pathEvaluatorSun2Moon = new PathAnimatorInflater.PathDataEvaluator(currentSunDataPath);
    mPathAnimator = ValueAnimator.ofObject(pathEvaluatorSun2Moon,
        PathParser.deepCopyNodes(mSunPath.getPathData()),
        PathParser.deepCopyNodes(moonPath.getPathData()));
  }

  @TargetApi(Build.VERSION_CODES.LOLLIPOP)
  public DayNightSwitch(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
    super(context, attrs, defStyleAttr, defStyleRes);
    init(context, attrs, defStyleAttr, defStyleRes);
  }

  @Override
  public void toggle() {
    setChecked(!isChecked());
  }

  @Override
  public void setChecked(boolean checked) {
    super.setChecked(checked);

    // Calling the super method may result in setChecked() getting called
    // recursively with a different value, so load the REAL value...
    checked = isChecked();

    if (isAttachedToWindow() && isLaidOut()) {
      animateThumbToCheckedState(checked);
    } else {
      // Immediately move the thumb to the new position.
      cancelPositionAnimator();
      setThumbPosition(checked ? 1 : 0);
    }
  }

  @Override
  protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);
  }

  ///////////////////////////////////////////////////
  @Override
  protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
    super.onLayout(changed, left, top, right, bottom);
  }

  @Override
  public boolean onTouchEvent(MotionEvent ev) {
    mVelocityTracker.addMovement(ev);
    final int action = ev.getActionMasked();
    switch (action) {
      case MotionEvent.ACTION_DOWN: {
        final float x = ev.getX();
        final float y = ev.getY();
        if (isEnabled()) {
          mTouchMode = TOUCH_MODE_DOWN;
          mTouchX = x;
          mTouchY = y;
        }
        break;
      }

      case MotionEvent.ACTION_MOVE: {
        switch (mTouchMode) {
          case TOUCH_MODE_IDLE:
            // Didn't target the thumb, treat normally.
            break;

          case TOUCH_MODE_DOWN: {
            final float x = ev.getX();
            final float y = ev.getY();
            if (Math.abs(x - mTouchX) > mTouchSlop ||
                Math.abs(y - mTouchY) > mTouchSlop) {
              mTouchMode = TOUCH_MODE_DRAGGING;
              getParent().requestDisallowInterceptTouchEvent(true);
              mTouchX = x;
              mTouchY = y;
              return true;
            }
            break;
          }

          case TOUCH_MODE_DRAGGING: {
            final float x = ev.getX();
            final int thumbScrollRange = getThumbScrollRange();
            final float thumbScrollOffset = x - mTouchX;
            float dPos;
            if (thumbScrollRange != 0) {
              dPos = thumbScrollOffset / thumbScrollRange;
            } else {
              // If the thumb scroll range is empty, just use the
              // movement direction to snap on or off.
              dPos = thumbScrollOffset > 0 ? 1 : -1;
            }

            final float newPos = MathUtils.constrain(mThumbPosition + dPos, 0, 1);
            if (newPos != mThumbPosition) {
              mTouchX = x;
              setThumbPosition(newPos);
            }
            return true;
          }
        }
        break;
      }

      case MotionEvent.ACTION_UP:
      case MotionEvent.ACTION_CANCEL: {
        if (mTouchMode == TOUCH_MODE_DRAGGING) {
          stopDrag(ev);
          // Allow super class to handle pressed state, etc.
          super.onTouchEvent(ev);
          return true;
        }
        mTouchMode = TOUCH_MODE_IDLE;
        mVelocityTracker.clear();
        break;
      }
    }

    return super.onTouchEvent(ev);
  }

  private int getThumbScrollRange() {
    return getWidth();
  }

  private void setThumbPosition(float position) {
    mThumbPosition = position;
    invalidate();
  }

  private void stopDrag(MotionEvent ev) {
    mTouchMode = TOUCH_MODE_IDLE;

    // Commit the change if the event is up and not canceled and the switch
    // has not been disabled during the drag.
    final boolean commitChange = ev.getAction() == MotionEvent.ACTION_UP && isEnabled();
    final boolean oldState = isChecked();
    final boolean newState;
    if (commitChange) {
      mVelocityTracker.computeCurrentVelocity(1000);
      final float xvel = mVelocityTracker.getXVelocity();
      if (Math.abs(xvel) > mMinFlingVelocity) {
        newState = xvel > 0;
      } else {
        newState = getTargetCheckedState();
      }
    } else {
      newState = oldState;
    }

    if (newState != oldState) {
      playSoundEffect(SoundEffectConstants.CLICK);
      setChecked(newState);
    }

    cancelSuperTouch(ev);
  }

  private boolean getTargetCheckedState() {
    return mThumbPosition > 0.5f;
  }

  private void cancelSuperTouch(MotionEvent ev) {
    MotionEvent cancel = MotionEvent.obtain(ev);
    cancel.setAction(MotionEvent.ACTION_CANCEL);
    super.onTouchEvent(cancel);
    cancel.recycle();
  }

  private void animateThumbToCheckedState(boolean newCheckedState) {
    final float targetPosition = newCheckedState ? 1 : 0;

    mPositionAnimator = ObjectAnimator.ofFloat(this, SUN_POS, targetPosition);
    mPositionAnimator.setDuration(THUMB_ANIMATION_DURATION);
    mPositionAnimator.setAutoCancel(true);
    mPositionAnimator.start();
  }

  private void cancelPositionAnimator() {
    if (mPositionAnimator != null) {
      mPositionAnimator.cancel();
    }
  }

  @Override
  public void draw(Canvas c) {
    // Draw the background.
    super.draw(c);
  }
}
